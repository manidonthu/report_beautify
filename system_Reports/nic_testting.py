#!/usr/bin/python
import os

def run_command(command):
        os.system(command+' > tmp')
        output = open('tmp','r').read()
        os.remove('tmp')
        return output

def _bond(n):
        if(os.path.isfile("/proc/net/bonding/"+n)):
                mode = run_command("grep Mode /proc/net/bonding/"+n)
                model = mode.split(":")[1].strip()
                addr = run_command("ifconfig "+n+" | grep inet")
                data = addr.split()
                inet_addr = ""
                Bcast = ""
                Mask = ""
                for i in range(len(data)):
                        if (data[i].strip()== "inet"):
                                inet_addr = data[i+1]
                                Bcast = data[i+2]
                                Mask = data[i+3]
                slaves = run_command("grep 'Slave Int' /proc/net/bonding/"+n+" | grep -V Primary")
                print n+" "+model+" \n TCPIP: "+inet_addr.split(":")[1]+" / "+Mask.split(":")[1]+" \n "+slaves+"\n"

print "\n\nBONDING Information\n"
print "-----------------------------------------------------------------------"
nic = run_command("ifconfig -a | grep HW")
for i in nic.split("\n"):
        if(len(i.strip()) == 0):
                pass
        else:
            data = i.strip().split()
            n = data[0]
            mac = data[-1]
            driver = run_command("ethtool -i" + n)
            print driver
            if(n.startswith("bond")):
                _bond(n)
