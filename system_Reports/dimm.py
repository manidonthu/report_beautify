#!/usr/bin/python
import subprocess
def run_command(command):
    output = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()[0]
    return output

hname = run_command('hostname')
print "\n Hostname: ", hname ,"\n"

dimm_output = run_command('dmidecode -t memory')
print "%20s %10s %10s %10s %10s %10s %10s" % ("Size","Location","Type","Speed","Manufacturer","Serial Num","Part Num")
print "---------------------------------------------------------------------------------"
for i in dimm_output.split("Handle 0")[1:]:
        default_option = "nil"
        #default_option = ''
        size = default_option
        locator = default_option
        type_ = default_option
        speed = default_option
        manuf = default_option
        serial = default_option
        part_num = default_option
        for j in i.split("\n"):
                j = j.strip()
                if j.startswith("Size"):
                        size = j.split(":")[1]
                if j.startswith("Locator"):
                        locator = j.split(":")[1]
                if j.startswith("Type:"):
                        type_ = j.split(":")[1]
                if j.startswith("Speed"):
                        speed = j.split(":")[1]
                if j.startswith("Manufacturer"):
                        manuf = j.split(":")[1]
                if j.startswith("Serial"):
                        serial = j.split(":")[1]
                if j.startswith("Part Num"):
                        part_num = j.split(":")[1]
        print "%20s %10s %10s %10s %10s %10s %10s" % (size,locator,type_,speed,manuf,serial,part_num)
