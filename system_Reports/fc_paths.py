#!/usr/bin/python
#===========================#
import sys
import os
#===========================#
fcdev = {}
rhv = ""
#===========================#
def run_command(command):
    os.system(command+' > tmp')
    output = open('tmp','r').read()
    os.remove('tmp')
    return output
#===========================#
def _Redhat5_6():
    print "Checking Multipath Modules\n"
    modules = run_command("lsmod")
    vxdmp_version = ""
    pp_version = ""
    devclass = ""
    for i in modules.split("\n"):
        if "vxdmp" in i:
            vxdmp_version = run_command("modinfo vxdmp -F version").strip()
            print " VxDMP "+vxdmp_version+" loaded in memory\n"
        if "emcp" in i:
            pp_version = run_command("modinfo emcp -F description").strip()
            print " "+pp_version+" loaded in memory\n"
    print "Unable to find the Multipath Modules\n"
    fc_info = run_command("systool -vc fc_host")
    for i in fc_info.split("\n"):
        if "Class Device =" in i:
                devclass = i.split("=")[1].strip()
                devclass = devclass.split('"')[1]
                fcdev[devclass] = {}
                fcdev[devclass]["devclass"] = devclass
                fcdev[devclass]["tape"] = "FALSE"
                ctlr = "c"+devclass.split("host")[1].strip()
                fcdev[devclass]["controller"] = ctlr
                if(vxdmp_version):
				mpath_type = run_command("vxdmpadm listctlr ctlr="+ctlr+" | grep ENA 1>/dev/null")
				if("ENABLE" in mpath_type):
					fcdev[devclass]["mpath"] = "VxDMP"
				else:
					fcdev[devclass]["mpath"] = "OS"
				fcdev[devclass]["tape"] = _hasTape(devclass)
        if "Class Device path =" in i:
                devpath = i.split("=")[1].strip()
                devpath = devpath.split('"')[1]
                fcdev[devclass]["dev_path"] = devpath
        if i.strip().startswith("Device path ="):
            bus_path = i.split("=")[1].strip()
            bus_path = bus_path.split('"')[1]
            fcdev[devclass]["bus_path"] = bus_path
            slot = bus_path.split(":")[3]
            card = bus_path.split(":")[4]
            port = bus_path.split(":")[5]
            fcdev[devclass]["port_num"] = port
            fcdev[devclass]["card_num"] = card
            fcdev[devclass]["slot_num"] = slot
        if "fabric_name" in i:
                fabric_name = i.split("=")[1]
                fabric_name = fabric_name.split('"')[1]
                fcdev[devclass]["fabric_name"] = fabric_name
        if "port_id" in i:
                port_id = i.split("=")[1].strip()
                port_id = port_id.split('"')[1]
                port_id = port_id.split("0x")[0]+port_id.split("0x")[1]
                if(port_id == "000000"):
                        fcdev[devclass]["port_id"] = "------"
                else:
                        fcdev[devclass]["port_id"] = port_id
        if "port_name" in i:
                port_name = i.split("=")[1].strip()
                port_name = port_name.split('"')[1]
                fcdev[devclass]["port_name"] = port_name
        if "port_state" in i:
                port_state = i.split("=")[1].strip()
                port_state = port_state.split('"')[1]
                fcdev[devclass]["port_state"] = port_state
        if "symbolic_name" in i:
                symbolic_name = i.split("=")[1].strip()
                symbolic_name = symbolic_name.split('"')[1]
                hba = symbolic_name.split()[0]
                fcdev[devclass]["hba_type"] = hba
        if i.strip().startswith("speed"):
                speed = i.split("=")[1].strip()
                speed = speed.split('"')[1]
                speed = speed.replace("it", "")
                if ("unknown" in speed):
                                fcdev[devclass]["speed"] = "0Gb"
                                fcdev[devclass]["mpath"] = "UNSET"
                else:
                    fcdev[devclass]["speed"] = speed
                    if("6." in rhv):
                        array_string = fcdev[devclass]["dev_path"]+"/../../rport\*/fc_remote_ports/rport\*/port_name"
                    else:
                        array_string = fcdev[devclass]["dev_path"]+"/device/rport\*/\*rport\*/port_name"
                        foo = run_command("echo "+array_string+" sed 's/0x//g'").strip()
                        fcdev[devclass]["arrays"] = foo
#===========================#
def _printRHEL5_6(fcdev):
    if len(fcdev)<=0:
        print "No class devices found"
    else:
        print "\nMapping WWPN to HBA Dev Path\n"
        for key in fcdev:
            if(fcdev[key]["speed"].strip()=="0Gb"):
                pass
            else:
                pci = fcdev[key]["bus_path"].strip()
                pci = pci.split("/sys/devices/")[1]
                print pci+" "+fcdev[key]["port_num"]+" "+fcdev[key]["port_name"]+" connected\n"
        print "\n"
        f_dev_path = "CTRL"
        f_port_name = "WWPN"
        f_speed = "Speed"
        f_port_id = "D_ID"
        f_mpath = "MPATH"
        f_arrays = "Storage Array WWPN"
        print "%10s %20s %5s %6s %6s %16s" % (f_dev_path,f_port_name,f_speed,f_port_id,f_mpath,f_arrays)
        for key in fcdev:
            f_dev_path = fcdev[key]["controller"]
            f_port_name = fcdev[key]["port_name"]
            f_speed = fcdev[key]["speed"]
            f_port_id = fcdev[key]["port_id"]
            f_mpath = fcdev[key]["mpath"]
            f_arrays = "----------------"
            if(f_speed=="0Gb"):
                f_arrays = "----- NONE -----"
            else:
                f_arrays = fcdev[key]["arrays"]
                print "%10s %20s %5s %6s %6s %16s" % (f_dev_path,f_port_name,f_speed,f_port_id,f_mpath,f_arrays)
        print "\n\n"
#===========================#
def _LinuxVersion():
    redhat_release = run_command("cat /etc/redhat-release")
    if(len(redhat_release)>0):
            isvm = run_command("dmidecode | grep 'VMWare Virtual Platform'").strip()
            if ("VM" in isvm):
                    print "VMWare Images are not supported\n"
                    sys.exit(1)
            rhv = run_command("head -1 /etc/redhat-release").strip()
            if ("5." in rhv) or ("6." in rhv):
                    print "\n"+rhv+" - fully supported\n"
                    print "\nNative DM Multipathing is handled per LUN. Check individual\n"
                    print "Luns for Mpath settings. VxDMP may be enabled on controller \nbut disabled per LUN \n"
                    _Redhat5_6()
                    _printRHEL5_6(fcdev)
            else:
                    print "\n"+rhv+" -No Supported\n"
                    sys.exit(1)
    else:
            print "OS not currently supported\n"
            sys.exit(1)
#===========================#
def _hasTape(devclass):
    tape = run_command("systool -vc scsi_generic -A "+devclass+" | grep ARCHIVE | head -1")
    if(len(tape)>0):
            return "TRUE"
    else:
            return "FALSE"
#===========================#
id = run_command('id')
if "uid=0" in id:
    pass
else:
    print "Run the script as root"
    sys.exit(1)
#===========================#
_LinuxVersion()