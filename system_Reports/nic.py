#!/usr/bin/python
import os
from tabulate import tabulate

def run_command(command):
        os.system(command+' > tmp')
        output = open('tmp','r').read()
        os.remove('tmp')
        return output

def _bond(n):
        if(os.path.isfile("/proc/net/bonding/"+n)):
                mode = run_command("grep Mode /proc/net/bonding/"+n)
                model = mode.split(":")[1].strip()
                addr = run_command("ifconfig "+n+" | grep inet")
                data = addr.split()
                inet_addr = ""
                Bcast = ""
                Mask = ""
                for i in range(len(data)):
                        if (data[i].strip()== "inet"):
                                inet_addr = data[i+1]
                                Bcast = data[i+2]
                                Mask = data[i+3]
                slaves = run_command("grep 'Slave Int' /proc/net/bonding/"+n+" | grep -V Primary")
                print n+" "+model+" \n TCPIP: "+inet_addr.split(":")[1]+" / "+Mask.split(":")[1]+" \n "+slaves+"\n"

print "\n\nBONDING Information\n"
print "-----------------------------------------------------------------------"
nic = run_command("ifconfig -a | grep HW")
for i in nic.split("\n"):
        if(len(i.strip()) == 0):
                pass
        else:
            data = i.strip().split()
            n = data[0]
            mac = data[-1]
            driver = run_command("ethtool -i "+n)
            if(n.startswith("bond")):
                _bond(n)

print('-' * 190)
print ('{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}'.format('DEV','OS_MAC_ADDR','UP','SPEED','DUPLEX','DRIVER','BUS','BOND','HW_MAC_ADDR'))
print('-' * 190)
nic = run_command("ifconfig -a | grep HW")
for i in nic.split("\n"):
	if(len(i.strip()) == 0):
        pass
    else:
		data = i.strip().split()
	    n = data[0]
	    mac = data[-1]
	    speed = ""
	    link = ""
	    dup = ""
	    state = run_command("ethtool " + n)
	    for j in state.split("\n"):
	    	k = j.strip()
			if k.startswith("Speed"):
				speed = k.split(":")[1].strip()
			if k.startswith("Link"):
				link = k.split(":")[1].strip()
			if k.startswith("Duplex"):
				dup = k.split(":")[1].strip()
		if(n.startswith("bond")):
			pass
		else:
			driver = run_command("ethtool -i " + n)
			driver_data = driver.split("\n")
			for i in driver_data:
	            if "driver:" in i:
	               	driver = i.split(":")[1].strip()
	            if "bus-info:" in i:
	                businfo = i.split(":", 1)[1].strip()
	        	if(os.path.isfile("/etc/sysconfig/network-scripts/ifcfg-"+n)):
    	        	master = run_command("grep MASTER /etc/sysconfig/network-scripts/ifcfg-"+n)
	            	if "=" in master:
						master = master.split("=")[1].strip()
                	hwa = run_command("grep HWADD /etc/sysconfig/network-scripts/ifcfg-"+n)
                	if "=" in hwa:
	                    hwa = hwa.split("=")[1].strip()
            print ('{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}{:^20}'.format(n,mac,link,speed,dup,driver,businfo,master,hwa))
            print('-' * 190)