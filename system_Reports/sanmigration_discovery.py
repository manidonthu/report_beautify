#!/usr/bin/python

import MySQLdb
import sys
if len(sys.argv)<=1:
        print "USAGE: ",sys.argv[0]," hostname"
        sys.exit(1)
hostname = sys.argv[1]

print "Hostname,Storage_Array,LDEV,Size,Function,Device,Slice,MPName,Group,Logical Name"

sql_query = "select hostname,serial,vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"

dssm = MySQLdb.connect(host="localhost",    # your host, usually localhost
        user="root",         # your username
        passwd="",  # your password
        db="dssm")        # name of the data base

cur = dssm.cursor()
cur.execute(sql_query)

for row in cur.fetchall():
        for i in range(len(row)-1):
                print row[i]+",",
        print row[-1]
dssm.close()