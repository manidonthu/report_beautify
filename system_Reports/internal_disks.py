#!/usr/bin/python
import os
def run_command(command):
        os.system(command+' > tmp')
        output = open('tmp','r').read()
        os.remove('tmp')
        return output

if os.path.isfile("/opt/dell/srvadmin/bin/omreport"):
        # server is a dell
        omreport_path = "/opt/dell/srvadmin/bin/omreport"
        controller = run_command(omreport_path+" storage controller | grep '^ID'")
        for item in controller.split("\n"):
                if(item.strip()== ""):
                        pass
                else:
                    ctlr = item.split(":")[1].strip()
                    print "\nInternal Array Overview of Controller ",ctlr,"\n"
                    adisk = run_command(omreport_path+" storage adisk controller="+ctlr)
                    vdisk = run_command(omreport_path+" storage vdisk controller="+ctlr)

        print "\n\tPhysical Disks on Controller ",ctlr,"\n"
        print "\t--------------------------------------------\n"
        for i in adisk.split("\nID")[1:]:
                i = "ID"+i
                default_option = "nil"
                a_id = default_option
                a_state = default_option
                a_size = default_option
                a_vid = default_option
                a_pid = default_option
                a_sn = default_option
                a_sas = default_option
                for j in i.split("\n"):
                        j = j.strip()
                        if j.startswith("ID"):
                                a_id = j.split(":")[1]
                        if j.startswith("State"):
                                a_state = j.split(":")[1]
                        if j.startswith("Capacity"):
                                a_size = j.split(":")[1]
                        if j.startswith("Vendor ID"):
                                a_vid = j.split(":")[1]
                        if j.startswith("Product ID"):
                                a_pid = j.split(":")[1]
                        if j.startswith("Serial No"):
                                a_sn = j.split(":")[1]
                        if j.startswith("SAS Address"):
                                a_sas = j.split(":")[1]
                print "\t",a_id," ",a_state," ",a_size," ",a_vid," ",a_pid," ",a_sn," ",a_sas
        print "\n\tVirtual Disks on Controller ",ctlr,"\n"
        print "\t--------------------------------------------\n"
        for i in vdisk.split("\nID")[1:]:
                i = "ID"+i
                default_option = "nil"
                v_id = default_option
                v_status = default_option
                v_size = default_option
                v_name = default_option
                v_disk_cache = default_option
                for j in i.split("\n"):
                        j = j.strip()
                        if j.startswith("ID"):
                                v_id = j.split(":")[1]
                        if j.startswith("Status"):
                                v_status = j.split(":")[1]
                        if j.startswith("Size"):
                                v_size = j.split(":")[1]
                        if j.startswith("Device Name"):
                                v_name = j.split(":")[1]
                        if j.startswith("Disk Cache"):
                                v_disk_cache = j.split(":")[1]
                print "\t",v_id," ",v_status," ",v_size," ",v_name," ",v_disk_cache
        print "\n"

if os.path.isfile("/usr/bin/hpacucli"):
        # HP Found
        hp = "/usr/bin/hpacucli"
        ctlr = run_command(hp+" ctrl all show").split()
        for i in range(len(ctlr)):
                if ctlr[i] == "Slot":
                        cnum = ctlr[i+1]
                        hp_pd_array = run_command(hp+" ctrl slot="+cnum+" pd all show")
                        hp_ld_array = run_command(hp+" ctrl slot="+cnum+" ld all show")

                        print "Physicaldrives on Slot ",cnum,"\n"
                        for j in hp_pd_array.split("\n"):
                                if "physicaldrive" in j:
                                        print j
                        print "Logicaldrives on Slot ",cnum,"\n"
                        for j in hp_ld_array.split("\n"):
                                if "logicaldrive" in j:
                                        print j
                else:
                        pass