#!/usr/bin/python
#=======================#
import sys
import os
#=======================#
def run_command(command):
    os.system(command+' > tmp')
    output = open('tmp','r').read()
    os.remove('tmp')
    return output
#=======================#
mpmap = {}
verbose = "false"
if "-v" in sys.argv:
    verbose = "true"
elif "verbose" in sys.argv:
    verbose = "true"
else:
    verbose = "false"
#=======================#
mpatha = run_command("multipath -v2 -ll | grep 36")
for line in mpatha.split("\n"):
    if(len(line.strip()) == 0):
        pass
    elif line.strip().startswith("("):
        text = line.split("(")[1]
        mpid = text.split(")")[0]
        mpmap[mpid]={"wwid":mpid,"mpath":"unconfig"}
    else:
        mp = line.split(" ")[0]
        text = line.split("(")[1]
        mpid = text.split(")")[0]
        mpmap[mpid]={"wwid":mpid,"mpath":mp}
#=======================#
vmware = run_command("dmidecode | grep VMware | head -1")
if("VMware,Inc." in vmware):
    print "vmDisplay"
#=======================#
if verbose == "true":
    print "%20s %10s %35s %10s %10s %10s %10s %10s %10s" % ("Hostname","Device","WWID","Lun_ID","Vendor","Model","Rev","MB_Size","Mpath")
elif ("VMware,Inc." in vmware):
    print "%10s %10s %10s %10s %10s" % ("Device","Vendor","Model","Rev","Size")
else:
    print "%10s %35s %10s %10s %10s %10s %10s %10s" % ("Device","WWID","Lun_ID","Vendor","Model","Rev","MB_Size","Mpath")
#=======================#
hostname = run_command("hostname").strip()
def _vmdisks():
    disklist = run_command("lsscsi | grep disk")
    for  line in disklist.split("\n"):
        line_split = line.split()
        vend = line_split[2].strip()
        model = line_split[3].strip()
        rev = line_split[-2].strip()
        disk = line_split[-1].strip()
        disk = disk.replace("/dev/","")
        size = int(run_command("cat /sys/block/"+disk+"/size").strip())
        size = ((size*512)/(1048576))
        print "%10s %10s %10s %10s %10s" % (disk,vend,model,rev,size)
#=======================#
if("VMware,Inc." in vmware):
    _vmdisks()
#=======================#
if (os.path.isdir("/dev/disk/by-id")):
    #This is for RHEL 5 or 6
    wwids = run_command("ls -l /dev/disk/by-id")
    mpath = ""
    for line in wwids.split("\n"):
        temp_var = line.split("scsi-")
        if(len(temp_var)>1):
            scsitmp = temp_var[1]
            scsiid = scsitmp.split("-> ../../")[0].strip()
            disk = scsitmp.split("-> ../../")[1].strip()
            if (len(disk)>3):
                pass
            else:
                lunid = scsiid[29:33].strip()
                vendor = run_command("cat /sys/block/"+disk+"/device/vendor").strip()
                model = run_command("cat /sys/block/"+disk+"/device/model").strip()
                rev = run_command("cat /sys/block/"+disk+"/device/rev").strip()
                size = int(run_command("cat /sys/block/"+disk+"/size").strip())
                size = ((size*512)/(1048576))
                if(scsiid in mpmap):
                    mpath = mpmap[mpid]['mpath']
                else:
                    mpath = "dm_excluded"
                if verbose == "true":
                    print "%20s %10s %35s %10s %10s %10s %10s %10s %10s" % (hostname,disk,scsiid,lunid,vendor,model,rev,str(size)+" MB",mpath)
                else:
                    print "%10s %35s %10s %10s %10s %10s %10s %10s" % (disk,scsiid,lunid,vendor,model,rev,str(size)+" MB",mpath)
#=======================#
else:
    wwids = run_command("ls -l /dev/disk/by-path")
    #This is for RHEL 4
    for line in wwids.split("\n"):
        lunid = ""
        scsiid = ""
        vendor = ""
        model = ""
        rev = ""
        mpath = ""
        disk = line.split("-> ../../")[1].strip()
        if (len(disk)>3):
            pass
        else:
            if (os.path.isfile("/csm/oss/bin/inq.LinuxAMD64"):
                lun = run_command("/csm/oss/bin/inq.LinuxAMD64 -et -r /dev/"+disk+" | grep "+disk).strip()
                scsiid = run_command("scsi_id -g -s /block/"+disk).strip()
                lunid = lun.split(":")[1].strip()
            else:
                scsiid = run_command("scsi_id -g -s /block/"+disk).strip()
                lunid = scsiid[29:33]
            if (os.path.isfile("/sys/block/"+disk+"/device/vendor")):
                vendor = run_command("cat /sys/block/"+disk+"/device/vendor").strip()
                model = run_command("cat /sys/block/"+disk+"/device/model").strip()
                rev = run_command("cat /sys/block/"+disk+"/device/rev").strip()
                size = int(run_command("cat /sys/block/"+disk+"/size").strip())
                size = ((size*512)/(1048576))
                if(scsiid in mpmap):
                    mpath = mpmap[mpid]['mpath']
                else:
                    mpath = "dm_excluded"
                if verbose == "true":
                    print "%20s %10s %35s %10s %10s %10s %10s %10s %10s" % (hostname,disk,scsiid,lunid,vendor,model,rev,str(size)+" MB",mpath)
                else:
                    print "%10s %35s %10s %10s %10s %10s %10s %10s" % (disk,scsiid,lunid,vendor,model,rev,str(size)+" MB",mpath)
            else:
                pass
#=======================#
