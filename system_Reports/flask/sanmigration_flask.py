import sanmigration_discovery
from flask import Flask,render_template,request

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("welcome.html")

@app.route("/sanmigration_discovery/<hostname>")
def get(hostname):
    result = sanmigration_discovery.san(hostname)
    return render_template("sanmigration_discovery.html", result=result)

if __name__ == '__main__':
    app.run('0.0.0.0','5678')