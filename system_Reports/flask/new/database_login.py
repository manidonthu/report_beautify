import os, MySQLdb
from flask import Flask, render_template, request, session, jsonify, json

hostname = ""
username = ""
password = ""
database = ""

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("welcome.html")

@app.route('/sanmigration_discovery')
def database():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:    
        password = request.form['password']
        username = request.form['username']
        hostname = request.form['hostname']
        database = request.form['databasename']

        db = MySQLdb.connect(host="oseportaldb.wellsfargo.com",
                user=username,
                passwd=password,
                db=database)

        cur = db.cursor()
        query = "select hostname,serial,vendor,ldev,ltype,device,mname,lname,lgroup from disks where hostname like '"+hostname+"' and ldev not like '-' group by ldev order by ldev;"
        cur.execute(query)
        return jsonify(data=cur.fetchall())

@app.route('/login', methods=['POST'])
def login():
    if MySQLdb.connect("oseportaldb.wellsfargo.com", username, password):
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return database()

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run('0.0.0.0',5678)
