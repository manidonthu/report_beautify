#!/usr/bin/python

import argparse
import MySQLdb
import socket
import sys, traceback
import os.path
import re
import xml.etree.ElementTree as etree
import datetime
import getpass
import urllib2

parser = argparse.ArgumentParser(description='This is a demo script for WFinq.')
parser.add_argument('-p', '--puppet', help='Puppet Facts', action='store_true')
parser.add_argument('-t', '--type', help='Connection type: hcm, portal')
parser.add_argument('-r', '--packages', help='RPMs and PKGS', action='store_true')
parser.add_argument('-S', '--server', help='Server to Run on')
parser.add_argument('-s', '--system', help='Syatem info (sysctl)', action='store_true')
parser.add_argument('-o', '--overview', help='Overview of the server', action='store_true')
parser.add_argument('-O', '--output_format', help='Output format: char, xml, json')
parser.add_argument('-m', '--module', help='OSE Portal module: file_fstab, file_hosts, file_nsswitch, file_resolv, file_mnttab, module_boot, module_disks, module_filesystem, module_cluster, module_network, module_applications, module_system')
parser.add_argument('-c', '--cluster', help='Cluster Info', action='store_true')
parser.add_argument('-H', '--hba', help='HBA Info', action='store_true')
parser.add_argument('-d', '--disks', help='Disk Info', action='store_true')
parser.add_argument('-n', '--nics', help='NIC Info', action='store_true')
parser.add_argument('-R', '--remedy', help='Remedy Info', action='store_true')
parser.add_argument('-i', '--issues', help='Cluster Issues Info', action='store_true')
args = parser.parser_arga()

if (re.match("hcm", args.type)) or (re.match("portal", args.type)):
	username = getpass.getuser()
	pwstr = 'AD-ENT\\' + username + ' Password: '
	Password = getpass.getpass(pwstr)
#In Complete Data
theurl = 'https://wdvri00a001.wellsfargo.com/wfqdata.py'
passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
passman.add_password(None, theurl, username, Password)
authhandler = urllib2.HTTPBasicAuthHandler(passman)
opener = urllib2.build_opener(authhandler)
urllib2.install_opener(opener)
pagehandle = urllib2.urlopen(theurl)
data = pagehandle.read()
exec(data)

if args.server:
	host_name=args.server
else:
	host_name=socket.gethostname()

if re.match("hcm", args.type):
	cur = HCMdb.cursor()
	if args.remedy:
		cur.execute("select HPSA_Mesh,Remote_Console_IP,Serial,BoKs_KEON,BMC_Firmware,DNS_Domain,DRAC,Model,OS_Type,OS_Version,OS_Version_Minor from HCM_CA_data where Server_name='" + host_name + "'")
	if args.hba:
		hba_str="FC_HBA_Info1,FC_HBA_Info2,FC_HBA_Info3,FC_HBA_Info4,FC_HBA_Info5,FC_HBA_Info6,FC_HBA_Info7,"
		hba_str+="FC_HBA_Info8,FC_HBA_Info9,FC_HBA_Info10"

		hb_str="SELECT "+ hba_str + " From HCM_CA_data where Server_Name='" + host_name + "'"
		cur.execute(hb_str)

if re.match("portal", args.type):
	cur = db.cursor()
	if args.module:
		cur.execute("select " + args.module + " from server_audit_details where host_name='" + host_name + "'")
	if args.packages:
		cur.execute("select * from pkgs where hostname='" + host_name + "'")
	if args.puppet:
		cur.execute("select * from puppet_fact where hostname='" + host_name + "'")
	if args.overview:
		cur.execute("select hostname,audit_date,os_type,os_version,os_release,stage,srv_use,customer,realm,manufacturer,model from server_audit_details where hostname='" + host_name + "'")
	if args.disks:
		cur.execute("select hostname,audit_date,serial,wwpn,vendor,ldev,port,size,device from disks where host_name='" + host_name + "'")
	if args.nics:
		cur.execute("select * from nics where hostname='" + host_name + "'")
	if args.system:
		cur.execute("select * from syscheck where hostname='" + host_name + "'")
	if args.issues:
		cur.execute("select * from cluster_issues_view where hostname='" + host_name + "'")
	if args.cluster:
		cur.execute("select * from cluster_res where hostname='" + host_name + "'")
	if args.hba:
		cur.execute("select * from hba_list where hostname='" + host_name + "'")
	if args.remedy:
		cur.execute("select CI_ID,SERIALNUMBER,LINE_OF_BUSINESS,OWNED,ASSET_LIFECYCLE_STATUS,SYSTEMROLE,OPERATING_SYSTEM_SUPPORT,DATABASE_SUPPORT,REMOTE_ACCESS_HARDWARE from p2k where hostname='" + host_name + "'")

result = cur.fetchall()
widths = []
columns = []
clen = 0
clenc = 1

for cd in cur.description:
	widths.append(max(cd[2],len(cd[0])))
	columns.append(cd[0])
clen = len(columns)
if args.output_format:
	for i in args.__dict__:
		if args.__dict__[i] is True:
			subitem=i
	if re.match("xml",args.output_format):
		for row in results:
			for item in range(0,clen):
				print ("<%s name=\"%s\" value=\"%s\"/>" % (subitem, columns[item],row[item]))
	else:
		for row in results:
			for item in range(0,clen):
					print ("%17s: %30s" % (columns[item],row[item]))
			print ("")
	else:
			tavnit = "|"
			separator = "+"
			for w in widths:
				tavnit += " %-"+"%ss |" % (w,)
				separator += '-'*w + '--+'
			if not (args.module):
				print(separator)
				print(tavnit % tuple(columns))
				print(separator)
			for row in results:
				print(tavnit % row)
			if not (args.module):
				print(separator)