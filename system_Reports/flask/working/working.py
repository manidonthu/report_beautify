import sys, re, MySQLdb, config
from flask import Flask, request, render_template, jsonify

host = config.host
user = config.username
passwd = config.password
db = config.database

regex = re.compile(r'[^a-zA-Z0-9]')

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template('form.html')

@app.route('/', methods=['POST'])
def my_form_post():
    form_hostname = request.form['hostname']
    hostname = form_hostname.strip()
    if (regex.search(hostname) == None):
        dbs = MySQLdb.connect(host,user,passwd,db)
        sql_query = config.query(hostname)
        cur = dbs.cursor()
        cur.execute(sql_query)
        count = cur.execute(sql_query)
        if count > 0:
            data = cur.fetchall()
        else:
            return hostname+" - Unable to find Hostname is Database"
    else:
        return hostname+" - Invalid HostName provided as input"
    return database(data)

@app.route("/")
def database(data):
    return render_template('template.html', data=data)

if __name__ == "__main__":
    app.run('0.0.0.0',5678,debug=True)
