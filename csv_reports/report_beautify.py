import csv
import glob
import os
import sys
import pandas as pd
import numpy as np

# process_file function defnition
def process_file(csvfile, pivot_table_row, pivot_table_row_count):
    # Creating the xlsx file name from CSV file provided in command-line argument or from the for loop(Bulk Actions)
    xlsxfile = csvfile[:-4] + '.xlsx'
    # Reading the provided CSV file and converting as XLSX file
    pd.read_csv(csvfile).to_excel(xlsxfile)
    # Reading the XLSX file for modifications
    df = pd.read_excel(xlsxfile, index_col=0)
    # Creating a pivot table for the user specified coloumns
    table = pd.pivot_table(df, index=pivot_table_row, values=pivot_table_row_count, aggfunc=lambda x: len(x.unique()))
    # Creating a writer to save the modifocations back to XLSX file
    writer = pd.ExcelWriter(xlsxfile)
    # Write the original data to the XLSX file with sheet name Data
    df.to_excel(writer, sheet_name='Data')
    # Write the pivot table to the XLSX file with sheet name Pivot_Table
    table.to_excel(writer, sheet_name='Pivot_Table')
    # Save all our modifications
    writer.save()

# To process multiple CSV files Use this loop to iterate.
def multiple_files():
    if len(sys.argv) == 3:
        for csvfile in glob.glob(os.path.join('.', '*.csv')):
            # Reading inputs from command-line arguments passed to the script
	    pivot_table_row = (sys.argv[1])
	    # Reading inputs from command-line arguments passed to the script
	    pivot_table_row_count = (sys.argv[2])
	    #Calling the process_file function
            process_file(csvfile, pivot_table_row, pivot_table_row_count)
    else:
        for csvfile in glob.glob(os.path.join('.', '*.csv')):
            # Reading inputs from command-line arguments passed to the script
            pivot_table_row = "Cluster Type"
            # Reading inputs from command-line arguments passed to the script
	    pivot_table_row_count = "Hostname"
	    #Calling the process_file function
            process_file(csvfile, pivot_table_row, pivot_table_row_count)

# To process single CSV file uncomment the Use the Below
def single_file():
    if len(sys.argv) == 4:
        csvfile = (sys.argv[1])
        # Reading inputs from command-line arguments passed to the script
        pivot_table_row = (sys.argv[2])
        # Reading inputs from command-line arguments passed to the script
        pivot_table_row_count = (sys.argv[3])
        #Calling the process_file function
        process_file(csvfile, pivot_table_row, pivot_table_row_count)
    elif len(sys.argv) == 2:
        csvfile = (sys.argv[1])
        # Reading inputs from command-line arguments passed to the script
        pivot_table_row = "Cluster Type"
        # Reading inputs from command-line arguments passed to the script
        pivot_table_row_count = "Hostname"
        #Calling the process_file function
        process_file(csvfile, pivot_table_row, pivot_table_row_count)
    else:
        print "Usage: python", (sys.argv[0]), "<your CSV file input>"

multiple_files()
#single_file()
