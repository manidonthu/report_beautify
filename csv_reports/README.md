# report_beautify.py

report_beautify.py will do the following.

1. Imports all CSV file from current or specified directory iside the for loop.
2. Save the contents of CSV into XLSX with the sheet name Data.
3. Hilights the first row.
4. Creates the pivot table in the same excel file with sheet name Pivot_Table.

# To process single CSV file
## python report_beautify.py butify_report.csv "Cluster Type" "Hostname"
#OR
## If no input provided the output will print Nothing

The Cluster Type is the row in your pivot table and Hostname is count

# To process multiple CSV files
## python report_beautify.py "Cluster Type" "Hostname"
#OR
## python report_beautify.py

The Cluster Type is the row in your pivot table and Hostname is count

