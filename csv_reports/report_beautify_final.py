#!/usr/bin/python

import csv
import glob
import os
import sys
import pandas as pd
import numpy as np
import argparse
from argparse import ArgumentParser

def process_file(csvfile, pivot_table_row, pivot_table_row_count):
    xlsxfile = csvfile[:-4] + '.xlsx'
    pd.read_csv(csvfile).to_excel(xlsxfile)
    df = pd.read_excel(xlsxfile, index_col=0)
    table = pd.pivot_table(df, index=pivot_table_row, values=pivot_table_row_count, aggfunc=lambda x: len(x.unique()))
    writer = pd.ExcelWriter(xlsxfile)
    df.to_excel(writer, sheet_name='Data')
    table.to_excel(writer, sheet_name='Pivot_Table')
    writer.save()

def multiple_files(csv_files_path, pivot_table_row, pivot_table_row_count):
    for csvfile in glob.glob(os.path.join(csv_files_path, '*.csv')):
        process_file(csvfile, pivot_table_row, pivot_table_row_count)

def single_file(csv_fiile_name, pivot_table_row, pivot_table_row_count):
    process_file(csv_fiile_name, pivot_table_row, pivot_table_row_count)

def main():
    parser = argparse.ArgumentParser(description='Convert CSV to XSLX & Create Pivot table')
    parser.add_argument('--singlefile',  action='store_true',  help='Create XLSX files with pivot tables with the provided files as input')
    parser.add_argument('--multiplefiles',  action='store_true',  help='Create XLSX files with pivot tables under the perticular directory if there are multiple CSV files')
    parser.add_argument('-r', '--table_row', type=str, default='Cluster Type', nargs='*', help='Pivot table row')
    parser.add_argument('-c', '--row_count', type=str, default='Hostname', nargs='*', help='Pivot row count')
    parser.add_argument('-p', '--file_path', type=str, help='CSV files Path i case of to process multiple files')
    parser.add_argument('-f', '--file_name', type=str, help='CSV file name as input')
    
    args = parser.parse_args()
    if len(sys.argv) <= 1:
        print("Please provide command-line arguments to generate pivot table")
        parser.print_help()
        exit(1)

    if args.singlefile:
        csv_fiile_name = args.file_name
        pivot_table_row = args.table_row
        pivot_table_row_count = args.row_count
        single_file(csv_fiile_name, pivot_table_row, pivot_table_row_count)
    elif args.multiplefiles:
        csv_files_path = args.file_path
        pivot_table_row = args.table_row
        pivot_table_row_count = args.row_count
        multiple_files(csv_files_path, pivot_table_row, pivot_table_row_count)
    else:
        parser.print_help()
        exit(1)        

main()
